
local root = "Interface\\AddOns\\rogwigs\\"
local path = root .. "%d.mp3"

BigWigsAPI:RegisterCountdown("Rog", {
	path:format(1),
	path:format(2),
	path:format(3),
	path:format(4),
	path:format(5),
	path:format(6),
	path:format(7),
	path:format(8),
	path:format(9),
	path:format(10)
})

local media = LibStub("LibSharedMedia-3.0")
local sound = media.MediaType and media.MediaType.SOUND or "sound"
media:Register(sound, "rogwigs: small ding", root .. "ding1.mp3")
media:Register(sound, "rogwigs: medium ding", root .. "ding2.mp3")
media:Register(sound, "rogwigs: large ding", root .. "ding3.mp3")
media:Register(sound, "rogwigs: beware", root .. "beware.mp3")
media:Register(sound, "rogwigs: destruction", root .. "destruction.mp3")
media:Register(sound, "rogwigs: dying", root .. "dying.mp3")
media:Register(sound, "rogwigs: proud", root .. "proud.mp3")
